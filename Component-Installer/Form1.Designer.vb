﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.chkOCX = New System.Windows.Forms.CheckBox()
        Me.chkDLL = New System.Windows.Forms.CheckBox()
        Me.rdoSystem = New System.Windows.Forms.RadioButton()
        Me.rdoSystem32 = New System.Windows.Forms.RadioButton()
        Me.rdoSyswow64 = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblCount = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(151, 102)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(116, 36)
        Me.btnStart.TabIndex = 0
        Me.btnStart.Text = "Start!"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'chkOCX
        '
        Me.chkOCX.AutoSize = True
        Me.chkOCX.Checked = True
        Me.chkOCX.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOCX.Location = New System.Drawing.Point(188, 22)
        Me.chkOCX.Name = "chkOCX"
        Me.chkOCX.Size = New System.Drawing.Size(48, 17)
        Me.chkOCX.TabIndex = 1
        Me.chkOCX.Text = "OCX"
        Me.chkOCX.UseVisualStyleBackColor = True
        '
        'chkDLL
        '
        Me.chkDLL.AutoSize = True
        Me.chkDLL.Checked = True
        Me.chkDLL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDLL.Location = New System.Drawing.Point(242, 22)
        Me.chkDLL.Name = "chkDLL"
        Me.chkDLL.Size = New System.Drawing.Size(46, 17)
        Me.chkDLL.TabIndex = 2
        Me.chkDLL.Text = "DLL"
        Me.chkDLL.UseVisualStyleBackColor = True
        '
        'rdoSystem
        '
        Me.rdoSystem.AutoSize = True
        Me.rdoSystem.Location = New System.Drawing.Point(188, 65)
        Me.rdoSystem.Name = "rdoSystem"
        Me.rdoSystem.Size = New System.Drawing.Size(59, 17)
        Me.rdoSystem.TabIndex = 3
        Me.rdoSystem.Text = "System"
        Me.rdoSystem.UseVisualStyleBackColor = True
        '
        'rdoSystem32
        '
        Me.rdoSystem32.AutoSize = True
        Me.rdoSystem32.Checked = True
        Me.rdoSystem32.Location = New System.Drawing.Point(253, 65)
        Me.rdoSystem32.Name = "rdoSystem32"
        Me.rdoSystem32.Size = New System.Drawing.Size(71, 17)
        Me.rdoSystem32.TabIndex = 4
        Me.rdoSystem32.TabStop = True
        Me.rdoSystem32.Text = "System32"
        Me.rdoSystem32.UseVisualStyleBackColor = True
        '
        'rdoSyswow64
        '
        Me.rdoSyswow64.AutoSize = True
        Me.rdoSyswow64.Location = New System.Drawing.Point(330, 65)
        Me.rdoSyswow64.Name = "rdoSyswow64"
        Me.rdoSyswow64.Size = New System.Drawing.Size(76, 17)
        Me.rdoSyswow64.TabIndex = 5
        Me.rdoSyswow64.Text = "Syswow64"
        Me.rdoSyswow64.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(163, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Which files should be registered?"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(166, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Where should they be registered?"
        '
        'lblCount
        '
        Me.lblCount.AutoSize = True
        Me.lblCount.Location = New System.Drawing.Point(161, 152)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(97, 13)
        Me.lblCount.TabIndex = 6
        Me.lblCount.Text = "Ready to execute.."
        Me.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(419, 174)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblCount)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.rdoSyswow64)
        Me.Controls.Add(Me.rdoSystem32)
        Me.Controls.Add(Me.rdoSystem)
        Me.Controls.Add(Me.chkDLL)
        Me.Controls.Add(Me.chkOCX)
        Me.Controls.Add(Me.btnStart)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "Form1"
        Me.Text = "Main - component Installer (vDCW©2018)"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnStart As Button
    Friend WithEvents chkOCX As CheckBox
    Friend WithEvents chkDLL As CheckBox
    Friend WithEvents rdoSystem As RadioButton
    Friend WithEvents rdoSystem32 As RadioButton
    Friend WithEvents rdoSyswow64 As RadioButton
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblCount As Label
End Class
