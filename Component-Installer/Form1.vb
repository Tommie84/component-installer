﻿
Imports Microsoft.VisualBasic.FileIO.FileSystem
Imports System.IO


Public Class Form1

    Dim cmdLine As String = "regsvr32.exe <file.ext>"
    Dim registered As Integer = 0
    Dim totalFiles As Integer = 0
    Dim errors As Integer = 0

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        resetCounters()

        For Each item As String In GetFiles(Application.StartupPath)
            Try
                Dim file As New FileInfo(item)
                totalFiles += 1
                If checkfilters(File.Extension) Then 'check to see if we need to process the file based on extension
                    Dim target As String = getDestination(file.Name)
                    CopyFile(file.FullName, target, True)
                    If register(target) Then registered += 1
                End If
                lblCount.Text = totalFiles & " files processed, " & registered & " registered, " & totalFiles - registered & " skipped.." & errors & " errors.. "
            Catch ex As Exception
                errors += 1
            End Try
        Next

        lblCount.Text += "... Done!"


    End Sub

    Public Function checkfilters(ByVal extension As String)
        If extension.ToLower = ".ocx" And chkOCX.Checked = True Then Return True
        If extension.ToLower = ".dll" And chkDLL.Checked = True Then Return True
        Return False
    End Function

    Public Function getDestination(ByVal originalFile As String)
        If rdoSystem.Checked Then Return "C:\windows\system\" & originalFile
        If rdoSystem32.Checked Then Return "C:\windows\system32\" & originalFile
        If rdoSyswow64.Checked Then Return "C:\windows\syswow64\" & originalFile
        Return "C:\windows\system32\" & originalFile
    End Function

    Public Function register(file As String)
        Try
            Shell("regsvr32.exe /s " & file)
            Return True
        Catch
            Return False
        End Try
    End Function

    Public Sub resetCounters()
        registered = 0
        totalFiles = 0
        errors = 0
    End Sub

End Class
